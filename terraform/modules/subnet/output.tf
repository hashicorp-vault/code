output "aws_subnet" {
    value = [aws_subnet.vault_publicsubnet.id, aws_subnet.vault_privatesubnet.id]
}

