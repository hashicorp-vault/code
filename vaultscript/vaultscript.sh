#!/bin/bash


ACTION=$1

#Install vault in Debian
installAtDebian(){
        sudo apt update && sudo apt install gpg
        wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null
        gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
        echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
        sudo apt update && sudo apt install vault
}

#Install vault in Redhat
installAtRedhat(){
        sudo yum update
        sudo yum install gpg
        sudo yum install -y yum-utils
        sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
        sudo yum -y install vault
}



#Remove vault in Debian
removeAtDebian(){
        echo removing
        sudo apt-get remove --purge vault

}

#Remove vault in Redhat
removeAtRedhat(){
        echo removing
        sudo yum remove vault
}

#Conditions
case $ACTION in
       install)
                if [ "$(. /etc/os-release; echo $NAME)" = "Ubuntu" ]; then
                        installAtDebian
                else
                        installAtRedhat
                fi
                ;;
        remove)
                if [ "$(. /etc/os-release; echo $NAME)" = "Ubuntu" ]; then
                        removeAtDebian
                elif [ "$(. /etc/os-release; echo $NAME)" = "CentOS" ]; then
                        removeAtRedhat
                fi
                ;;
             *)
                echo "wrong input"
                ;;
esac
